# Bootstrap

This repository contains Bootstrap examples that I have used to implement features in my projects. They are usually the result of not being able to find a clear example elsewhere.

*  bs4_delete_confirmation_modal.html
    *  Delete Confirmation with Modal Window
	*  Can be used to confirm any action such as save, delete etc
	*  Data can be passed from Button to Modal window such as the object name and the URL to follow when the user wishes to proceed with the action.
	
# References

*  [Official Bootstrap Site](https://getbootstrap.com/)
*  [Official Documentation](https://getbootstrap.com/docs/4.3/getting-started/introduction/)
*  [w3schools Bootstrap 4 Tutorials](https://www.w3schools.com/bootstrap4/)